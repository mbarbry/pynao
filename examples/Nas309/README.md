# Sodium cluster with ghost atoms

Sodium cluster containing 147 Na atoms plus one layer of ghost atoms.
This configuration was used to get the data presented in Chapter 5 of Ref. [1]

Comparison with the old `MBPT_LCAO` code is shown on figure `mbpt_VS_pynao_Nas309.svg`
(Ask Peter koval koval.peter@gmail.com, or Marc Barbry marc.barbry@mailoo.org)
to access the original data (stored on Koval-00,
`/scratch/share/data-backup/Na_clusters/good/eps-0.15/ghost/custom_basis/Na309_lc5.05`)

File `Polarizability_inter.npy-ref20200804` contains reference polarizability data
calculated with PyNAO on 2020-08-04 (master branch).

## Run the example

To run the example, just perform the DFT calculations with Siesta

    siesta < siesta.fdf > siesta.out

run the TDDFT calculation by

    python runtddft.py

The script will calculate the polarizability along the x direction. The file
polarizability matrix will be written to `Polarizability_inter.npy`.
When you plot the polarizability (see file `mbpt_VS_pynao_Nas309.svg`), the
plasmon frequency should be around 3.1 eV.

You can then obtain the spatial distribution of the density change with the script
`get_spatial_dist.py`

    python get_spatial_dist.py

The density change will be written to `dn_spatial_optical_w3.10eV.npy`.

## References

1. Plasmons in Nanoparticles: Atomistic Ab Initio Theory for Large Systems, M. Barbry,
(https://cfm.ehu.es/cfm_news/phd-thesis-defense-marc-barbry/)
