{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TDDFT: Silver cluster\n",
    "\n",
    "In this tutorial, we will see how to calculate the polarizability of a silver cluster of 147 atoms using TDDFT method.\n",
    "\n",
    "In this folder you should find the following files\n",
    "\n",
    "* `siesta.fdf`: the siesta input file to run DFT calculation.\n",
    "* `geometry.siesta.fdf`: The geometry of the system for siesta.\n",
    "* `Ags147.xyz`: The geometry of the system in xyz format. The ghost atoms are represented as normal silver atoms.\n",
    "* `Ag.psf`: pseudopotential for silver atoms\n",
    "* `Ags.psf`: pseudopotential for ghost atoms\n",
    "\n",
    "The inputs files can be downloaded from this [link](https://mbarbry.website.fr.to/pynao/resources/Ags309.zip)\n",
    "\n",
    "## Run Siesta calculations\n",
    "\n",
    "The first step is to launch siesta calculation to get the inputs for TDDFT.\n",
    "This can be done by (assuming `siesta` executable is in your `PATH`)\n",
    "\n",
    "    siesta < siesta.fdf > siesta.out\n",
    "    \n",
    "To run directly from the notebook, you need to add `!` before the call as shown n the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!siesta < siesta.fdf > siesta.out"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run TDDFT calculations\n",
    "\n",
    "### Sytem initialization\n",
    "Once Siesta calculation finished, we have all the necessary inputs to launch the TDDFT calculations.\n",
    "We first need to initialize the system.\n",
    "\n",
    "PyNAO must be able to find inputs from siesta, this can be done by setting the parameter `label`. It must be the same than the value `SystemLabel` found in `siesta.fdf`. The directory for the calculation is given by the parameter `cd`.\n",
    "\n",
    "Other parameters are optional, the most important ones are\n",
    "\n",
    "* `krylov_solver`: the Krylov solver type used for solving the linear system of equation\n",
    "* `krylov_options`: The options for the solver. A tolerance of `10e-5` is often enough to get accurate results.\n",
    "* `level`: To control the number of radial and angular grids (see [`pyscf.dft.Grids`](http://pyscf.org/pyscf/dft.html?highlight=grids#pyscf.dft.gen_grid.Grids))\n",
    "* `krylov_solver`: Iterative solver to use to solve the linear system of equation see [SciPy page](https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html)\n",
    "* `krylov_options`: The options for the Krylov solver\n",
    "* `iter_broadening`: The broadening applied to the polarizability. The broadening should not be smaller than $\\frac{3}{2}d\\omega$, where $d\\omega$ is the frequency step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "\n",
    "from ase.units import Ry, eV, Ha\n",
    "from pynao import tddft_iter\n",
    "\n",
    "# Run TDDFT\n",
    "dname = os.getcwd() \n",
    "\n",
    "# Calculate the kernel\n",
    "eps = 0.15\n",
    "td = tddft_iter(label='siesta', cd=dname, verbosity=4, krylov_solver=\"lgmres\",\n",
    "                krylov_options={\"tol\": 1.0e-5, \"atol\": 1.0e-5},\n",
    "                iter_broadening=eps/Ha, tol_loc=1e-4, tol_biloc=1e-6, jcutoff=7,\n",
    "                xc_code='LDA,PZ', level=0, GPU=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Rough calculations of the polarizability\n",
    "\n",
    "We can now calculate the polarizability of the system. In this example we will \n",
    "\n",
    "* First do a rough calculation to get a approximation to the excitation over a braod frequency range\n",
    "* calculate for a fewer frequencies a more accurate solution around the excitation peak.\n",
    "\n",
    "The aim is to reduce the amount of total iteration necessary to define the surface dipole mode."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reduce the tolerance for faster convergence (at the price of accuracy)\n",
    "td.krylov_options[\"tol\"] = 1.0e-3\n",
    "td.krylov_options[\"atol\"] = 1.0e-3\n",
    "\n",
    "freq = np.arange(0.0, 10.0, 0.2)/Ha + 1.0j*td.eps\n",
    "p_mat = td.comp_polariz_inter_Edir(freq, Eext=np.array([1.0, 0.0, 0.0]), tmp_fname=\"pol.tmp\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can analyze the rough polarizability obtained"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "np.save(\"freq_range_rough.npy\", freq.real)\n",
    "np.save(\"polarizability_inter_rough.npy\", p_mat)\n",
    "\n",
    "iw = np.argmax(-p_mat[0, 0, :].imag)\n",
    "print(\"max(Pxx) = {}\".format(freq.real[iw]*Ha))\n",
    "\n",
    "h = 9\n",
    "w = 4*h/3\n",
    "ft = 20\n",
    "lw = 4\n",
    "\n",
    "fig = plt.figure(1, figsize=(w, h))\n",
    "ax = fig.add_subplot(111)\n",
    "\n",
    "ax.plot(freq.real*Ha, -p_mat[0, 0, :].imag, linewidth=lw)\n",
    "ax.set_xlabel(r\"Energy (eV)\", fontsize=ft)\n",
    "ax.set_ylabel(r\"$P_{xx}$ (a.u.)\", fontsize=ft)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see from the graph of $P_{xx}$ is aroung 3.5 eV.\n",
    "\n",
    "### Accurate calculation of the excitation peak\n",
    "\n",
    "We can now do more accurate calculations on a reduced frequency range (between 3.3 and 3.7 eV) with a higher tolerance for the iterative solver."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "td.krylov_options[\"tol\"] = 1.0e-5\n",
    "td.krylov_options[\"atol\"] = 1.0e-5\n",
    "\n",
    "freq2 = np.arange(3.3, 3.7, 0.01)/Ha + 1.0j*td.eps\n",
    "p_mat2 = td.comp_polariz_inter_Edir(freq2, Eext=np.array([1.0, 0.0, 0.0]), tmp_fname=\"pol.tmp\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.save(\"freq_range_acc.npy\", freq2.real)\n",
    "np.save(\"polarizability_inter_acc.npy\", p_mat2)\n",
    "\n",
    "iw = np.argmax(-p_mat2[0, 0, :].imag)\n",
    "print(\"max(Pxx) = {}\".format(freq2.real[iw]*Ha))\n",
    "\n",
    "fig = plt.figure(2, figsize=(w, h))\n",
    "ax = fig.add_subplot(111)\n",
    "ax.plot(freq.real*Ha, -p_mat[0, 0, :].imag, linewidth=lw)\n",
    "ax.plot(freq2.real*Ha, -p_mat2[0, 0, :].imag, linewidth=lw)\n",
    "ax.set_xlabel(r\"Energy (eV)\", fontsize=ft)\n",
    "ax.set_ylabel(r\"$P_{xx}$ (a.u.)\", fontsize=ft)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have a much more accurate description of the surface dipole peak, which is located at 3.51 eV.\n",
    "\n",
    "## Electronic density change\n",
    "Lets calculate the electronic density change distribution $\\delta n$ in space to vizualize the surface dipole."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.units import Bohr\n",
    "from pynao.m_comp_spatial_distributions import spatial_distribution\n",
    "\n",
    "# From `siesta.out` we can get the box size\n",
    "# siesta: Automatic unit cell vectors (Ang):\n",
    "# siesta:   30.214756    0.000000    0.000000\n",
    "# siesta:    0.000000   30.214756    0.000000\n",
    "# siesta:    0.000000    0.000000   30.214756\n",
    "\n",
    "box = np.array([[-16.0, 16.0],\n",
    "                [-16.0, 16.0],\n",
    "                [-16.0, 16.0]])/Bohr\n",
    "\n",
    "\n",
    "dr = np.array([0.3, 0.3, 0.3])/Bohr\n",
    "\n",
    "# initialize spatial calculations\n",
    "# see file `pynao.m_comp_spatial_distributions` for some description of the inputs\n",
    "# be aware that the density change in product basis, nao_td.dn could be loaded\n",
    "# from file, allowing you to change the spatial density distribution parameters\n",
    "# without redoing the full calculation\n",
    "# One mus also be careful to the inputs of `spatial_distribution`\n",
    "# they must be the same the the function used to calculate `td.dn`\n",
    "spd = spatial_distribution(td.dn, freq2, box, dr=dr, label=\"siesta\",\n",
    "                           excitation=\"light\",cd=dname, verbosity=4,\n",
    "                           tol_loc=1e-4, tol_biloc=1e-6, jcutoff=7,\n",
    "                           level=0)\n",
    "\n",
    "# compute spatial density change distribution at a specific frequency\n",
    "w0 = 3.51/Ha\n",
    "spd.get_spatial_density(w0)\n",
    "\n",
    "np.save(\"dn_iter_spatial.npy\", spd.dn_spatial)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig2 = plt.figure(2, figsize=(w, h))\n",
    "ax = fig2.add_subplot(111)\n",
    "\n",
    "idx = 54 # middle of the system\n",
    "vmax = np.max(abs(spd.dn_spatial[:, :, 54].imag))\n",
    "vmin = -vmax\n",
    "ext = [box[0, 0], box[0, 1], box[1, 0], box[1, 1]]\n",
    "ax.imshow(spd.dn_spatial[:, :, 54].imag, cmap=\"seismic\", vmin=vmin, vmax=vmax, interpolation=\"bicubic\", origin=\"lower\", extent=ext)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The surface plasmon is clearly visible at the cluster surface. We can also see very well the density change created by the d-electron of the silver clusters.\n",
    "\n",
    "Lets now calculate the distribution of the electric field enhancement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## compute Efield\n",
    "Efield = spd.comp_induce_field()\n",
    "np.save(\"Efield_tem_spatial.npy\", Efield)\n",
    "\n",
    "## compute intensity\n",
    "intensity = spd.comp_intensity_Efield(Efield)\n",
    "np.save(\"Efield_intensity_tem_spatial.npy\", intensity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig3 = plt.figure(3, figsize=(w, h))\n",
    "ax = fig3.add_subplot(111)\n",
    "\n",
    "vmax = np.max(abs(intensity[:, :, 54]))\n",
    "vmin = 0.0\n",
    "print(vmin, vmax)\n",
    "ax.imshow(intensity[:, :, 54], vmin=vmin, vmax=vmax/3, interpolation=\"bicubic\", origin=\"lower\", extent=ext)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
