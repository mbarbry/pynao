"""
Calculate the spatial distribution of the density change calculated from a previous
tddft_iter or tddft_tem calculation
"""

import os
import numpy as np
from pynao.m_comp_spatial_distributions import spatial_distribution
from ase.units import Ha, Bohr

z = 0.01
# File name in which the density change in product basis is stored
fname_density = "density_change_prod_basis_inter_tem_zshift_{0:.2f}Bohr.npy".format(z)
#fname_density = "density_change_prod_basis_inter_optical.npy"

eps = 0.15/Ha
# The frequency range for which the density change has been calculated
freq = np.arange(0.0, 10.0, 0.05)/Ha

# boundary of the box for calculating the spatial distribution
# look at the cell in siesta.out to get an idea of how large the box should be
lb = -22
up = 22
box = np.array([[lb, up],
                [lb, up],
                [lb, up]])/Bohr

# The accuracy along x, y, and z axes
dr = np.array([0.3, 0.3, 0.3])/Bohr

# load the density change
dn = np.load(fname_density)
dname = os.getcwd()

# initialize the system
spd = spatial_distribution(dn, freq, box, dr=dr, label="siesta",
                           cd=dname, verbosity=4, iter_broadening=eps, tol_loc=1e-4,
                           tol_biloc=1e-6, jcutoff=7, xc_code='LDA,PZ', excitation="electron")

# Calculate the density change for the frequencies omegas
# look at the spectrum to know the resonance frequencies
omegas = np.array([3.85])
for w0 in omegas:
    print("Calculate spatial distribution density change at {0:.2f} eV".format(w0))
    spd.get_spatial_density(w0/Ha)
    np.save("dn_tem_spatial_z{0:.2f}Bohr_w{1:.2f}eV_new.npy".format(z, w0), spd.dn_spatial)
    #np.save("dn_optical_spatial_w{0:.2f}eV.npy".format(w0), spd.dn_spatial)
