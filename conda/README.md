# Build Conda package

instructions to build and upload the conda package

    conda-build . --output-folder ./build
    anaconda login
    anaconda upload build/linux-64/pynao-0.1.2-py312_0.tar.bz2

Package can then be installed by creating a new environment to avoid dependencies conflicts

    conda create -n pynao mbarbry::pynao
