from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import mf as mf_c
from pyscf import gto, scf

class KnowValues(unittest.TestCase):

    def test_0069_vnucele_coulomb_water_ae(self):
        """
        This
        """
        mol = gto.M(verbose=1, atom='O 0 0 0; H 0 0.489 1.074; H 0 0.489 -1.074',
                    basis='cc-pvdz')
        gto_mf = scf.RHF(mol)
        gto_mf.kernel()
        vne = mol.intor_symmetric('int1e_nuc')
        dm_gto = gto_mf.make_rdm1()
        E_ne_gto = (vne*dm_gto).sum()*0.5
        Eref = -97.67613224691334
        self.assertAlmostEqual(E_ne_gto, Eref)

        tk = mol.intor_symmetric('int1e_kin')
        E_kin_gto = (tk*dm_gto).sum()
        Eref = 75.37551630632431
        self.assertAlmostEqual(E_kin_gto, Eref)

        mf = mf_c(gto=mol, mf=gto_mf)
        vne_nao = mf.vnucele_coo_coulomb().toarray()
        dm_nao = mf.make_rdm1().reshape((mf.norbs, mf.norbs))
        E_ne_nao = (vne_nao*dm_nao).sum()*0.5
        Eref = -97.67613148066803
        self.assertAlmostEqual(E_ne_nao, Eref)

        tk_nao = -0.5*mf.laplace_coo().toarray()
        E_kin_nao = (tk_nao*dm_nao).sum()
        Eref = 75.37551630570162
        self.assertAlmostEqual(E_kin_nao, Eref)

if __name__ == "__main__":
    unittest.main()
