from __future__ import print_function, division
import os, unittest, numpy as np
from pynao import m_fft

def gaussian(x,sig):
    return np.pi**(-0.25)*sig**(-0.5)*np.exp(-x**2/(2*sig**2))

def fgaussian(k,sig):
    return np.pi**(-0.25)*sig**(0.5)*np.exp(-k**2*sig**2/2)*np.sqrt(2*np.pi)

class KnowValues(unittest.TestCase):

    def test_fft_gaussian(self):
        """
        Test the computation of Fourier transform of a Gaussian function given
        its positive part only.
        """
        N = 101
        x = np.linspace(0, 15, N)
        sig = 0.3
        y = gaussian(x, sig)

        x_extend, y_extend = m_fft.FTextend(x, y)

        k1 = m_fft.get_fft_freq(x_extend)
        fa = fgaussian(k1, sig)

        fa1 = m_fft.FT1(x_extend, y_extend, axis=0, norm='asym', d=0)
        ify = m_fft.iFT1(k1, fa1, axis=0, norm='asym', d=0)

        m1 = np.mean(np.abs(y - ify[N - 1:]))
        m2 = np.mean(np.abs(fa - fa1))
        self.assertTrue(np.allclose(m1, 0, atol=3.5e-5))
        self.assertTrue(np.allclose(m2, 0, atol=3.5e-5))

if __name__ == "__main__":
    unittest.main()
