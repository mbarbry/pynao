from __future__ import print_function, division
import unittest

class KnowValues(unittest.TestCase):

    def test_0170_tddft_iter_Na55(self):
        """
        Test the polarizability of a Na55 cluster
        Calculations originally done for the pyscf-nao paper.

        The results are slightly different that for the paper. The reasons
        are:
        
        * New energy shift (20 meV instead of 100 meV). This is because the
        calculated number of electrons with 100 meV was not good enough.
        * New Eshift => New relaxed geometry
        * correction of 0.5 for the electronic temperature from Siesta (see issue
        https://gitlab.com/mbarbry/pynao/-/issues/31)

        Ref:
            PySCF-NAO: An efficient and flexible implementation of linear response
            time-dependent density functional theory with numerical atomic orbitals,
            P. Koval, M. Barbry and D. Sanchez-Portal, Computer Physics Communications,
            2019, 10.1016/j.cpc.2018.08.004
        """

        import os
        import numpy as np
        from pynao import tddft_iter

        dname = os.path.dirname(os.path.abspath(__file__)) + '/test_0170_Na55'

        # eps = 0.15 eV
        eps = 0.005512398372128198
        td = tddft_iter(label='siesta', cd=dname, verbosity=2,
                        iter_broadening=eps, tol_loc=1e-4, tol_biloc=1e-6,
                        jcutoff=7, xc_code='LDA,PZ', level=0,
                        krylov_options={"rtol": 5.0e-6, "atol": 1.0e-8})

        # freq = np.arange(0.0, 5.0, 0.05) in eV
        freq = np.arange(0.0, 0.18374661240427326, 0.0018374661240427327) + 1j*td.eps
        p0mat = td.comp_polariz_nonin_edir(freq, eext=np.array([1.0, 0.0, 0.0]))

        ref_P0 = np.load("{}/test_0170_tddft_iter_Na55_P0xx_ref.npy".format(dname))
        #np.save("{}/test_0170_tddft_iter_Na55_P0xx_ref.npy".format(dname), td.p0_mat)
        #print("diff P0:")
        #print(np.sum(abs(ref_P0.real - td.p0_mat.real))/td.p0_mat.size)
        #print(np.sum(abs(ref_P0.imag - td.p0_mat.imag))/td.p0_mat.size)

        # Changing just changing library error can introduce a mean error
        # up to 0.00186
        # if this test fails, check the polarizability graphically
        assert np.sum(abs(ref_P0.real - td.p0_mat.real))/td.p0_mat.size < 1.0e-2
        assert np.sum(abs(ref_P0.imag - td.p0_mat.imag))/td.p0_mat.size < 1.0e-2


        pmat = td.comp_polariz_inter_edir(freq, eext=np.array([1.0, 0.0, 0.0]))
        ref_P = np.load("{}/test_0170_tddft_iter_Na55_Pxx_ref.npy".format(dname))
        #np.save("{}/test_0170_tddft_iter_Na55_Pxx_ref.npy".format(dname), td.p_mat)
        #print("diff P:")
        #print(np.sum(abs(ref_P.real - td.p_mat.real))/td.p_mat.size)
        #print(np.sum(abs(ref_P.imag - td.p_mat.imag))/td.p_mat.size)
        #import matplotlib.pyplot as plt
        #plt.plot(freq.real, ref_P.imag[0, 0, :])
        #plt.plot(freq.real, td.p_mat.imag[0, 0, :], "--")
        #plt.show()

        assert np.sum(abs(ref_P.real - td.p_mat.real))/td.p_mat.size < 1.0e-2
        assert np.sum(abs(ref_P.imag - td.p_mat.imag))/td.p_mat.size < 1.0e-2

if __name__ == "__main__": 
    unittest.main()
