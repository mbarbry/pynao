from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import mf as mf_c
from pynao.m_siesta_units import siesta_conv_coefficients

class KnowValues(unittest.TestCase):

    def test_0074_vna_vnl_LiH(self):
        """
        reSCF then G0W0
        """

        dname = os.path.dirname(os.path.abspath(__file__))
        mf = mf_c(label='lih', cd=dname, forceCenterSystem=False)
        vna = mf.vna_coo(level=1).toarray()
        rdm = mf.make_rdm1()[0,0,:,:,0]
        Ena = siesta_conv_coefficients["ha2ev"]*(-0.5)*(vna*rdm).sum()
        # previous value for Ena: 6.0251828965429937
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # This compares badly with lih.out
        # siesta: Ena     =         9.253767
        self.assertAlmostEqual(Ena, 6.025455510807828)

        vnl = mf.vnl_coo().toarray()
        Enl = siesta_conv_coefficients["ha2ev"]*(vnl*rdm).sum()
        # previous value for Ena: -2.8533506650656162
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # This compares ok with lih.out
        # siesta: Enl     =        -2.853393
        self.assertAlmostEqual(Enl, -2.853350866384697)

if __name__ == "__main__":
    unittest.main()
