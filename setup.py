import os
import sys
import subprocess

import sysconfig
import warnings
from setuptools import setup, find_packages
from distutils.command.build_py import build_py as _build_py
from setuptools.command.install import install
from setuptools.command.build_ext import build_ext

def build_libraries(build_type="Release"):
    """
    Compile the C and Fortran code and install the libraries in the pynao folder

    For debugging use build_type="Debug", in that case, use the lib/cmake_user_inc_examples/cmake.user.inc-gnu-debug
    arch file for the compilation
    """

    root = os.path.dirname(os.path.abspath(__file__))

    conf_file = "lib/cmake.arch.inc"
    default_conf_file = "lib/cmake_user_inc_examples/cmake.user.inc-gnu-openmp"
    if os.path.isfile(conf_file):
        print("Using user defined configuration file ", conf_file)
    else:
        warnings.warn("Using default configuration file {}".format(default_conf_file))
        subprocess.call("cp {0}/{1} {0}/{2}".format(root, default_conf_file, conf_file),
                        shell=True)

    os.chdir(f"{root}/lib")
    try:
        os.mkdir("build")
    except:
        print("build folder already, delete everything under")
        subprocess.call("rm -r build/*", shell=True)

    os.chdir("build")
    cmd = f"cmake -DCMAKE_BUILD_TYPE={build_type} -DCMAKE_INSTALL_PREFIX={root}/pynao .."
    print("CMake: ", cmd)
    ret = subprocess.call(cmd, shell=True)
    if ret != 0:
        raise ValueError("cmake failed")
    ret = subprocess.call("make", shell=True)
    if ret != 0:
        raise ValueError("make failed")

    os.chdir("../..")

CLASSIFIERS = [
'Development Status :: Development',
'Intended Audience :: Developers',
'License :: OSI Approved :: BSD License',
'Programming Language :: Python, C, Fortran',
'Programming Language :: Python :: 3',
'Topic :: Software Development',
'Topic :: Scientific/Engineering',
'Operating System :: Linux',
]

NAME             = 'pynao'
MAINTAINER       = 'Marc Barbry, Peter Koval, Masoud Mansouri'
MAINTAINER_EMAIL = 'marc.barbry@mailoo.org, koval.peter@gmail.com, ma.mansoury@gmail.com'
DESCRIPTION      = 'PyNAO: Many-Body Perturbation Theory with Numerical Atomic Orbitals'
LICENSE          = 'BSD'
AUTHOR           = 'Marc Barbry, Peter Koval, Masoud Mansouri'
AUTHOR_EMAIL     = 'marc.barbry@mailoo.org, koval.peter@gmail.com, ma.mansoury@gmail.com'
PLATFORMS        = ['Linux']
requirements = ["numpy", "scipy>=1.0", "numba", "h5py", "pyscf"]
pyth_version = sys.version.split()[0]

if int(pyth_version.split(".")[0]) < 3:
    raise ValueError("PyNAO requires python3")

build_lib = True
if build_lib:
    build_libraries()

from pynao import __version__
setup(
    name=NAME,
    version=__version__,
    description=DESCRIPTION,
    license=LICENSE,
    classifiers=CLASSIFIERS,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    platforms=PLATFORMS,
    install_requires=requirements,
    python_requires='>=3.5',
    packages=['pynao'],
    package_dir={'pynao': 'pynao'},
    package_data={'pynao': ['*.dll', '*.dylib', '*.so']},
    #entry_points={"console_scripts": [
    #    "runAbinitio=abinitioLaunchers.runAbinitio:main",
    #    "runMulitpleGeo=abinitioLaunchers.runMulitpleGeo:main"]}
)
