from __future__ import print_function, division
from pynao.m_coulomb_am import coulomb_am
import numpy as np
import numba as nb
import scipy.sparse as sparse
from timeit import default_timer as timer

def comp_coulomb_sparse(sv, ao_log=None, funct=coulomb_am, dtype=np.float64,
                        threshold=0.0, **kvargs):
    """
    Computes the matrix elements given by funct, for instance coulomb interaction

    Input parameters:
    
    sv: System Variables
        this must have arrays of coordinates and species, etc

    Output parameters:
    
    res: 2D np.array, dtype
         matrix elements (real-space overlap) for the whole system
    """
    from pynao.m_ao_matelem import ao_matelem_c

    aome = ao_matelem_c(sv.ao_log.rr, sv.ao_log.pp)
    me = aome.init_one_set(sv.ao_log) if ao_log is None else aome.init_one_set(ao_log)
    atom2s = np.zeros((sv.natm+1), dtype=np.int32)
    for atom, sp in enumerate(sv.atom2sp):
        atom2s[atom+1] = atom2s[atom] + me.ao1.sp2norbs[sp]
    norbs = atom2s[-1]

    # dim triangular matrix: n*(n+1)/2
    #res = np.zeros((norbs, norbs), dtype=dtype)
    spmat = sparse.csr_matrix((norbs, norbs), dtype=dtype)
    ttotal = 0.0
    innz = 0
    for atom1, [sp1, rv1, s1, f1] in enumerate(zip(sv.atom2sp, sv.atom2coord, atom2s,
                                                   atom2s[1:])):
        t1 = timer()
        too2f = 0.0
        tfill = 0.0
        tmpmat = sparse.csr_matrix((norbs, norbs), dtype=dtype)
        for atom2, [sp2, rv2, s2, f2] in enumerate(zip(sv.atom2sp, sv.atom2coord,
                                                       atom2s, atom2s[1:])):
            if atom2 > atom1:
                continue
            
            oo2f = funct(me, sp1, rv1, sp2, rv2, **kvargs)
            rows, cols, data = fill_coo_matrix_tiangular(s1, s2, oo2f,
                                                             threshold=threshold)
            tmpmat += sparse.coo_matrix((data, (rows, cols)), dtype=dtype,
                                        shape=(norbs, norbs)).tocsr()
        spmat += tmpmat
        t2 = timer()
        ttotal += t2 - t1
        if sv.verbosity > 4:
            print("Hartee kernel timing: atm {}/{}: {} ".format(atom1, sv.natm,
                                                                t2-t1))

    if sv.verbosity > 2:
        print("{}\t====> Hartree kernel timing: {}".format(__name__, ttotal))

    return spmat

@nb.jit(nopython=True)
def count_nnz(arr, thres=1.0e-25):

    nnzs = 0
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            if abs(arr[i, j]) > thres:
                nnz += 1
    return nnz

@nb.jit(nopython=True)
def count_triangular_nnz(s1, s2, arr, thres=1.0e-25):

    nnz = 0
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            if s2+j <= s1+i and abs(arr[i, j]) > thres:
                nnz += 1
    return nnz


@nb.jit(nopython=True)
def fill_sparse(st1, st2, arr, innz, rows, cols, data, thres=1.0e-25):

    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            if abs(arr[i, j]) > thres:
                rows[innz] = st1 + i
                cols[innz] = st2 + j
                data[innz] = arr[i, j]
                innz += 1
    return innz

#@nb.jit(nopython=True)
def fill_csr_matrix_tiangular(s1, s2, block, shape, threshold=1.0e-14):

    nnz = count_triangular_nnz(s1, s2, block, thres=threshold)
    indices = np.zeros((nnz), dtype=np.int32)
    indptr = np.zeros((shape[0]+1), dtype=np.int32)
    data = np.zeros((nnz), dtype=block.dtype)

    innz = 0
    for i in range(block.shape[0]):
        count = 0
        for j in range(block.shape[1]):
            if s2+j <= s1+i and abs(block[i, j]) > threshold:
                #indices[innz] = s1 + i
                indices[innz] = s2 + j
                data[innz] = block[i, j]
                innz += 1
                count += 1
        indptr[s1+i+1] = count

    return indices, indptr, data

@nb.jit(nopython=True)
def fill_coo_matrix_tiangular(s1, s2, block, threshold=1.0e-14):
#def fill_coo_matrix_tiangular(s1, s2, block, innz, rows, cols, data,
#                              threshold=1.0e-14):
    nnz = count_triangular_nnz(s1, s2, block, thres=threshold)
    rows = np.zeros((nnz), dtype=np.int32)
    cols = np.zeros((nnz), dtype=np.int32)
    data = np.zeros((nnz), dtype=block.dtype)

    innz = 0
    for i in range(block.shape[0]):
        for j in range(block.shape[1]):
            if s2+j <= s1+i and abs(block[i, j]) > threshold:
                rows[innz] = s1 + i
                cols[innz] = s2 + j
                data[innz] = block[i, j]
                innz += 1

    return rows, cols, data
