import pynao.m_fft as ft
import numpy as np

def comp_dipole_moment_kick(omegas, polarizability, siesta_units=False,
                            kick_magnitude=1, eps=0):
    """
    Compute timedependent dipole moment for a delta-kick initial field

    Input Parameters:

    omegas: 1D np.array, float
        Frequency range for which the polarizability spectrum is calculated
        in atomic units.
    polarizability: 1D np.array, complex
        Polarizability calculated with tddft_iter class.
    siesta_units: bool
        If true, dipole moment units from Siesta are adopted.
    kick_magnitude: float
        Magnitude of the applied delta-kick in atomic units.
    eps: float
        Iterative broadening used in spectrum calculation.

    Output Parameters:

    dipole_moment: 1D np.array, complex
        Dipole moment in atomic units.
    """

    if siesta_units:
        spec = -1j*polarizability
    else:
        spec = polarizability
    omegatot, spectot = ft.FTextend(omegas, spec)
    inv = ft.iFT1(omegatot, spectot, d=eps)
    dipole_moment = inv[len(inv)//2:]*kick_magnitude
    return dipole_moment

def gaussian_pulse(time, amplitude, center_frequency, center_time, duration):
    """
    Compute electric field of a monochromatic Gaussian pulse. All units are
    atomic units.

    Input Parameters:

    time: 1D np.array, float
        Time instances for which the electric field is calculated.
    amplitude: float
        Amplitude of the pulse.
    center_frequency: float
        Center frequency of the pulse.
    center_time: float
        Time for which the electric field reaches the maximum value.
    duration: float
        Pulse duration.

    Output Parameters:

    omegas: 1D np.array, float
    """    

    gp = amplitude*np.cos(center_frequency*(time-center_time))*\
         np.exp(-(time-center_time)**2/duration**2)

    return gp


def comp_dipole_moment_gauss(omegas, polarizability, amplitude,
                             center_frequency, center_time, duration):
    """
    Compute timedependent dipole moment with a monochromatic Gaussian pulse as
    initial electric field.

    Input Parameters:

    omegas: 1D np.array, float
        Frequency range for which the polarizability spectrum is calculated
        in atomic units.
    polarizability: 1D np.array, complex
        Polarizability calculated with tddft_iter class.
    time: 1D np.array, float
        Time instances for which the electric field is calculated.
    amplitude: float
        Amplitude of the pulse.
    center_frequency: float
        Center frequency of the pulse.
    center_time: float
        Time for which the electric field reaches the maximum value.
    duration: float
        Pulse duration.

    Output Parameters:

    dipole_moment: 1D np.array, complex
        Dipole moment in atomic units.
    """
    omegatot, spectot = ft.FTextend(omegas, polarizability)

    tgp = ft.get_fft_freq(omegatot)
    gp = gaussian_pulse(tgp, amplitude, center_frequency, center_time,
                        duration)
    gpft_tot = ft.FT1(tgp, gp)
    p = ft.FTconvolve(spectot, gpft_tot, omegatot, domain='freq')

    return p[len(p)//2:]

def comp_dipole_strength_function(omegas, polarizability):
    """
    Compute dipole strength function given the polarizability spectrum.

    Input Parameters:

    omegas: 1D np.array, float
        Frequency range for which the polarizability spectrum is calculated
        in atomic units.
    polarizability: 1D np.array, complex
        Polarizability calculated with tddft_iter class.

    Output Parameters:

    dipole_strength_function: 1D np.array, float
        Dipole strength function in atomic units.
    """
    dipole_strength_function = 2 / np.pi * omegas * polarizability.imag

    return dipole_strength_function

def comp_time(omegas):
    """
    Compute time instances for which the dipole moment is calculated given a set
    of frequencies.

    Input Parameters:

    omegas: 1D np.array, float

    Output Parameters:

    t: 1D np.array, float
    """
    omegatot=np.concatenate((-omegas[1:], [0], omegas[1:]))
    omegatot=np.sort(omegatot)    
    t=ft.get_fft_freq(omegatot)
    t=t[len(t)//2:]

    return t

def comp_omegas(t):
    """
    Compute frequencies

    Input Parameters:

    t: 1D np.array, float

    Output Parameters:

    omegas: 1D np.array, float
    """
    w2 = 2 * np.pi * np.fft.ifftshift(
        np.fft.fftfreq(2 * len(t), t[1] - t[0]))
    omegas = w2[w2 >= 0]
    return omegas
