"""
Module to import the C and Fortran libraries
"""

from platform import system
import os
from ctypes import CDLL

sysname = system()
if sysname == "Darwin":
    ext = 'dylib'
elif sysname == 'Windows':
    ext = 'DLL'
else:
    ext = 'so'

root = os.path.dirname(os.path.abspath(__file__))
libsparsetools = CDLL(f"{root}/libsparsetools.{ext}")
libnao = CDLL(f"{root}/libnao.{ext}")
