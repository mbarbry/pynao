# 
# Copyright (C) 1996-2016	The SIESTA group
#  This file is distributed under the terms of the
#  GNU General Public License: see COPYING in the top directory
#  or http://www.gnu.org/copyleft/gpl.txt.
# See Docs/Contributors.txt for a list of contributors.
#
#-------------------------------------------------------------------
# arch.make file for gfortran compiler.
# To use this arch.make file you should rename it to
#   arch.make
# or make a sym-link.
# For an explanation of the flags see DOCUMENTED-TEMPLATE.make

.SUFFIXES:
.SUFFIXES: .f .F .o .c .a .f90 .F90

SIESTA_ARCH = unknown

CC = gcc
FPP = $(FC) -E -P -x c
FC = gfortran
FC_SERIAL = gfortran

INCLUDE_PATH = -I/usr/include -I/opt/programs/hdf5/include -I/opt/programs/netcdf/include -I/opt/programs/netcdf-fortran/include
FFLAGS = -O2 -fPIC -ftree-vectorize $(INCLUDE_PATH)

AR = ar
RANLIB = ranlib

SYS = nag

SP_KIND = 4
DP_KIND = 8
KINDS = $(SP_KIND) $(DP_KIND)

LDFLAGS = -fopenmp

BLAS = -lblas -llapack
SCALAPACK_LIBS =
COMP_LIBS = #libsiestaLAPACK.a libsiestaBLAS.a
COMP_LIBS += libncdf.a libfdict.a

FPPFLAGS = $(DEFS_PREFIX)-DFC_HAVE_ABORT -DCDF -DNCDF -DNCDF_4

#MPI_INTERFACE = libmpi_f90.a
#MPI_INCLUDE = .

LIBS = $(BLAS) $(SCALAPACK_LIBS)
LIBS += -L/opt/programs/netcdf/lib -L/opt/programs/netcdf-fortran/lib -L/opt/programs/hdf5/lib -lnetcdff -lnetcdf -lhdf5_fortran -lhdf5 -lz

# Dependency rules ---------

FFLAGS_DEBUG = -g -O1   # your appropriate flags here...

# The atom.f code is very vulnerable. Particularly the Intel compiler
# will make an erroneous compilation of atom.f with high optimization
# levels.
atom.o: atom.F
	$(FC) -c $(FFLAGS_DEBUG) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_fixed_F) $< 

.c.o:
	$(CC) -c $(CFLAGS) $(INCFLAGS) $(CPPFLAGS) $< 
.F.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_fixed_F)  $< 
.F90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_free_F90) $< 
.f.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FCFLAGS_fixed_f)  $<
.f90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FCFLAGS_free_f90)  $<

