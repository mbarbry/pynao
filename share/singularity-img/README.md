# Singularity recipe

Collection of singularity recipy to easily install pynao and its dependencies

    singularity build pynao.img <pynao-recipe>
    singularity shell pynao.img

Check https://sylabs.io/docs/ for more information about singularity.
At the moment the recipes are compatible with version 2.6 of singularity.

## List of recipes

* `pynao-sing.gnu`: Simple recipe installing pyscf via pip, and with GNU Blas
library. No OpenMP support (issue with pyscf pip package)
* `pynao-sing.mkl.gpu`: Performance focused recipy with MKL library. PySCF is
compiled manually to permit the OpenMP support in PyNAO.
