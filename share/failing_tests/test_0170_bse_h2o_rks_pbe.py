from __future__ import print_function, division
import os,unittest,numpy as np
from pyscf import gto, scf, tddft, dft
from pyscf.data.nist import HARTREE2EV
from pyscf.dft.libxc import  xc_type, parse_xc_name, is_hybrid_xc, is_gga, is_lda
from pynao.m_polariz_inter_ave import polariz_freq_osc_strength
from pynao import bse_iter

class KnowValues(unittest.TestCase):

    def test_170_bse_h2o_rks_b3lyp_pb(self):
    """ This  """
    mol = gto.M(verbose=1,atom='O 0 0 0; H 0 0.489 1.074; H 0 0.489 -1.074',basis='cc-pvdz')
    gto_mf = scf.RKS(mol)
    gto_mf.xc = 'PBE'
    gto_mf.kernel()
    veff = gto_mf.get_veff()
    #print(veff.shape)
    #print(veff.sum())
    #gto_td = tddft.TDDFT(gto_mf)
    #gto_td.nstates = 190
    #gto_td.kernel()
    
    #print(__name__, parse_xc_name(gto_mf.xc) )
    #print(__name__, xc_type(gto_mf.xc) )
    #print(__name__, is_hybrid_xc(gto_mf.xc) )
    #print(__name__, is_gga(gto_mf.xc) )
    #print(__name__, is_lda(gto_mf.xc) )
    
    return 
    
    raise RuntimeError('check ')
    
    omegas = np.arange(0.0, 2.0, 0.01) + 1j*0.03
    p_ave = -polariz_freq_osc_strength(gto_td.e, gto_td.oscillator_strength(), omegas).imag
    data = np.array([omegas.real*HARTREE2EV, p_ave])
    np.savetxt('test_0170_bse_h2o_rks_pbe_pyscf.txt', data.T, fmt=['%f','%f'])
    #data_ref = np.loadtxt('test_0170_bse_h2o_rks_pbe_pyscf.txt-ref').T
    #self.assertTrue(np.allclose(data_ref, data, atol=1e-6, rtol=1e-3))
    
    nao_td  = bse_iter(mf=gto_mf, gto=mol, verbosity=1)
    
    fxc = nao_td.comp_fxc_pack()
    print(fxc.shape)
    print(__name__, fxc.sum())
    
    polariz = -nao_td.comp_polariz_inter_ave(omegas).imag
    data = np.array([omegas.real*HARTREE2EV, polariz])
    np.savetxt('test_0170_bse_h2o_rks_pbe_nao.txt', data.T, fmt=['%f','%f'])
    #data_ref = np.loadtxt('test_0170_bse_h2o_rks_pbe_nao.txt-ref').T
    #self.assertTrue(np.allclose(data_ref, data, atol=1e-6, rtol=1e-3), \
    #  msg="{}".format(abs(data_ref-data).sum()/data.size))
    
if __name__ == "__main__": unittest.main()
