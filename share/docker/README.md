# Docker images: How to

## Build and push the image on gitlab

    docker login registry.gitlab.com
    docker build -t registry.gitlab.com/mbarbry/pynao .
    docker push registry.gitlab.com/mbarbry/pynao

For login, you may need to use a Personal Access Token, see
https://gitlab.com/help/user/profile/personal_access_tokens

## Build docker

    docker build --tag pynao .

## Run docker interactively

    docker run -v /hostmountpoint:/containermountpoint -it pynao bash

## Push image to hub.docker.com

    docker login
    docker build -f Dockerfile -t mycontainer
    docker tag mycontainer mbarbry/pynao:latest
    docker push mbarbry/pynao:latest

## Pull the image on your system and run

    docker pull mbarbry/pynao:latest
    docker run -p 8888:8888 -it mbarbry/pynao:latest

The container starts an jupyter notebook. You can connect to it with your web
browser via the address http://127.0.0.1:8888/lab 

The PyNAO tutorials are presents in the `tutorial-pynao` folder.

To run the docker with Nvidia GPUs enabled, follow the instructions on their
[website](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#id7),
and use the following command to run the docker

    docker run --rm --gpus all -p 8888:8888 -it mbarbry/pynao:latest

If previous command fails, try to run

    systemctl restart docker
